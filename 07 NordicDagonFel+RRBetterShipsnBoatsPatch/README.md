#### 07 NordicDagonFel+RRBetterShipsnBoatsPatch

##### About

Provide compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData/HD"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/00 - Main Files"
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/01 - Main ESP - English"
# OR, not both!
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/patches/26 RR Ships and Boats"
...
data="/home/username/games/openmw/Mods/FloraLandscape/OAABShipwrecks/00 Core"
# OR, not both!
data="/home/username/games/openmw/Mods/CitiesTowns/NordicDagonFel/00 Core"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/07 NordicDagonFel+RRBetterShipsnBoatsPatch"
...
content=Tamriel_Data.esm
...
content=OAAB_Data.esm
...
content=RR_Better_Ships_n_Boats_Eng.esp
...
content=Nordic Dagon Fel.ESP
content=NordicDagonFel+RRBetterShipsnBoatsPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/07%20NordicDagonFel%2BRRBetterShipsnBoatsPatch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
