#### 08 BCOM+TOTSP+RRBetterShipsnBoatsPatch

##### About

Provide compatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231), [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001), and [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810).

The BCOM-patched plugin for RR Mod Series - Better Ships and Boats will place dupe ships to the west of TOTSP's anthology landmass location, this plugin fixes them. **NOTE** that this does not work with the standalone version of RR Mod Series - Better Ships and Boats!

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData/HD"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/Core/00 Core"
...
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/00 - Main Files"
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/patches/26 RR Ships and Boats"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/08 BCOM+TOTSP+RRBetterShipsnBoatsPatch"
...
content=Tamriel_Data.esm
...
content=OAAB_Data.esm
...
content=Solstheim Tomb of The Snow Prince.esm
...
content=Beautiful cities of Morrowind.esp
...
content=RR_Better_Ships_n_Boats_Eng.esp
content=BCOM+TOTSP+RRBetterShipsnBoatsPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/08%20BCOM%2BTOTSP%2BRRBetterShipsnBoatsPatch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
