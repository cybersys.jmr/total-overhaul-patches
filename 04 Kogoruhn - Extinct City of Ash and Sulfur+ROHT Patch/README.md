#### 04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch

**NOTE: This patch is currently outdated and is not distributed with the rest of the patches. Sorry for any inconvenience!**

##### About

Provides compatibility between Kogoruhn - Extinct City of Ash and Sulfur and Rise of House Telvanni by disabling various objects when a certain plot point has been reached.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No
* [`Rise of House Telvanni - 2.0`](https://www.nexusmods.com/morrowind/mods/48225): Yes

##### Load Order

```
...
data="/home/larry/games/openmw/Mods/Landmasses/TamrielData/HD"
data="/home/larry/games/openmw/Mods/Landmasses/TamrielData/00 Core"
...
data="/home/larry/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/larry/games/openmw/Mods/ModdingResources/Dr_Data"
...
data="/home/larry/games/openmw/Mods/FloraLandscape/KogoruhnExtinctCityofAshandSulfur/Morrowind/Data Files"
data="/home/larry/games/openmw/Mods/Patches/TotalOverhaulPatches/04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch"
...
content=Rise of House Telvanni.esm
content=Tamriel_Data.esm
content=OAAB_Data.esm
content=Dr_Data.esm
...
content=Riharradroon - Path to Kogoruhn v1.0.ESP
...
content=Kogoruhn - Extinct City of Ash and Sulfur.esp
content=Kogoruhn - Extinct City of Ash and Sulfur.omwscripts
content=Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/04%20Kogoruhn%20-%20Extinct%20City%20of%20Ash%20and%20Sulfur%2BROHT%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

This patch is also featured on the [Kogoruhn - Extinct City of Ash and Sulfur mod files page](https://www.nexusmods.com/morrowind/mods/51615?tab=files)!
