#### 05 BCOM+GhastlyGG+DynamicDistantBuildingsPatch

##### About

An edited version of the `HM_DDD_Ghostfence_v1.0.esp` plugin from [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) that supports [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231).

**NOTE: This replaces the `HM_DDD_Ghostfence_v1.0.esp` plugin from Dynamic Distant Buildings for OpenMW and is meant to be loaded instead of it!**

**NOTE: The `BCOM+GhastlyGG+DynamicDistantBuildingsPatch.esp` plugin has already been cleaned! Some tooling (TES3CMD) may want to delete two fence records that it thinks are dirty. They look dirty, but they are not. If you clean those out this plugin will be broken!**

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No; dynamic distants are not supported as a feature on this platform
* `Rebirth`: No
* `Anything else that edits the related Ghostfence and/or Ghostgate cells`: Unknown, please let me know about compatibility issues!

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/core/00 Core"
...
data="/home/username/games/openmw/Mods/Fixes/DynamicDistantDetails"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/05 BCOM+GhastlyGG+DynamicDistantBuildingsPatch"
...
content=Beautiful cities of Morrowind.esp
...
content=ghastly gg.esp
...
content=BCOM+GhastlyGG+DynamicDistantBuildingsPatch.esp
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/05%20BCOM%2BGhastlyGG%2BDynamicDistantBuildingsPatch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
