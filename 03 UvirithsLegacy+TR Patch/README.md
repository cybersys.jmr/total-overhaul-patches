#### 03 UvirithsLegacy+TR Patch

##### About

Fixes compatibility between Uvirith's Legacy and Tamriel Rebuilt by deleting several objects that conflict with newer TR versions. Made for [this version](https://modding-openmw.com/mods/uviriths-legacy-35-tr-add-on-2101/) of the UL+TR patch.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No; the UL+TR plugin it's patched for was made for OpenMW and may not work with this platform.
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielRebuilt/00 Core"
...
data="/home/username/games/openmw/Mods/GuildsFactions/UvirithsLegacy"
...
data="/home/username/games/openmw/Mods/GuildsFactions/UvirithsLegacy35TRaddon2101"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/03 UvirithsLegacy+TR Patch"
...
content=Tamriel_Data.esm
content=TR_Mainland.esm
...
content=Uvirith's Legacy_3.53.esp
...
content=UL_3.5_TR_21.01_Add-on.esp
content=UvirithsLegacy+TR Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/03%20UvirithsLegacy%2BTR%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
