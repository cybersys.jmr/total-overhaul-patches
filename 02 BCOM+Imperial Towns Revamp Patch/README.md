#### 02 BCOM+Imperial Towns Revamp Patch

Supports BCOM version: **2.9.4 or newer**

##### About

Fixes the placement of a cartwheel object for [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Imperial Towns Revamp](https://www.nexusmods.com/morrowind/mods/49735).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/larry/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/core/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/ImperialTownsRevamp/00 - Data Files"
data="/home/username/games/openmw/Mods/CitiesTowns/ImperialTownsRevamp/00 - Interior cells"
data="/home/username/games/openmw/Mods/CitiesTowns/ImperialTownsRevamp/01 - LOD0"
data="/home/username/games/openmw/Mods/CitiesTowns/ImperialTownsRevamp/04 - OpenMW Spec. AND Normal Maps"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/02 BCOM+Imperial Towns Revamp Patch"
...
content=Beautiful cities of Morrowind.esp
content=BCOM+Imperial Towns Revamp Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/02%20BCOM%2BImperial%20Towns%20Revamp%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
